import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.base import MIMEBase

from email import encoders

def send():
    message_text = "Hello World"
    to = "tylercmoua@gmail.com"
    sender = "TylerAndPaulsCamera@gmail.com"
    senderPass= 'TP1234567890'
    subject = "Lab 6 Tyler Moua"

    message = MIMEMultipart(message_text)
    message['To'] = to
    message['From'] = sender
    message['subject'] = subject
    message.preamble = "Lab 6"

    msgText = MIMEText('<img src="cid:image1">', 'html')

    fp = open('picture.jpg', 'rb')
    msgImage = MIMEImage(fp.read())
    fp.close()
   

    msgText = MIMEText("Livestream(Enter into VLC Player): http://192.168.43.223:8160")

    message.attach(msgText)
    message.attach(msgImage)
    
    filename = "sample.wav"
    attachment = open("/home/pi/cpe-185-section-2-project/sample.wav", "rb")

    part = MIMEBase('application', 'octet-stream')
    part.set_payload((attachment).read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

    message.attach(part)
    

    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.starttls()
    s.login(sender, senderPass)
    s.sendmail(sender, to, message.as_string())
    s.quit()
    print("Email Sent!")
    return
    
