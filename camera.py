#!/usr/bin/env python

from picamera import PiCamera
from time import sleep

def snap():
    camera = PiCamera()
    camera.start_preview()
    sleep(1)
    camera.capture('/home/pi/cpe-185-section-2-project/picture.jpg')
    camera.stop_preview()
    return
