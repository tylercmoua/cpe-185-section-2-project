 #-------------------------------CODE 2
    # External module imports
import RPi.GPIO as GPIO
import time
import os
from camera import snap
from Email import send
# Pin Definitons:
def bell():
    buttonPin = 27 # Broadcom pin 17 (PI pin 13)
    # Pin Setup:
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(buttonPin, GPIO.IN)
    # Initial state for LED:
    print("Simple Python GPIO Start! Press CTRL+C to exit")
    try:
        while 1:
            if GPIO.input(buttonPin):
                print("high")
                snap()
                os.system("arecord --format=S16_LE --duration=5 --rate=16000 --file-type=raw sample.wav")
                time.sleep(3)
                send()
                print("Bell Finished")
                break
                #Call method to Take screenShot
    except KeyboardInterrupt: # If CTRL+C is pressed, exit and clean
        GPIO.cleanup() # cleanup all GPIO            
        
    return